import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { AdminGuard as UserIsAdmin } from './components/auth/admin.guard'
import { LoginGuard as isLogged } from './components/auth/login.guard'
import { LoginComponent } from './components/auth/login/login.component'
import { RegisterComponent } from './components/auth/register/register.component'
import { GearComponent } from './components/gear/gear.component'
import { GearListComponent } from './components/gear/gear-list/gear-list.component'
import { GearEditComponent } from './components/gear/gear-edit/gear-edit.component'
import { ItemComponent } from './components/item/item.component'
import { ItemListComponent } from './components/item/item-list/item-list.component'
import { ItemEditComponent } from './components/item/item-edit/item-edit.component'
import { ItemDetailsComponent } from './components/item/item-details/item-details.component'
import { CategoryEditComponent } from './components/item/category-edit/category-edit.component'
import { UsecaseComponent } from './components/usecase/usecase.component'

const routes: Routes = [
  
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', canActivate: [isLogged], component: DashboardComponent },
  { path: 'admin', canActivate: [UserIsAdmin], component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'usecase', component: UsecaseComponent },
  { path: 'gears', canActivate: [isLogged], component: GearComponent },
  { path: 'gear/:id', canActivate: [isLogged], component: GearListComponent },
  { path: 'gear', canActivate: [isLogged], component: GearListComponent },
  { path: 'gear/:id/edit', canActivate: [isLogged], component: GearEditComponent },
  { path: 'category', component: ItemComponent },
  { path: 'category/:id/edit', canActivate: [isLogged], component: CategoryEditComponent },
  { path: 'items/:id', component: ItemListComponent },
  {
    path: 'item/new',
    canActivate: [isLogged],
    component: ItemEditComponent,
    data: {
      itemAlreadyExists: false,
      title: 'New Item'
    }
  },
  { path: 'item/:id', component: ItemDetailsComponent },
  {
    path: 'item/:id/edit',
    canActivate: [isLogged],
    component: ItemEditComponent,
    data: {
      itemAlreadyExists: true,
      title: 'Edit Item'
    }
  },


  { path: '**', redirectTo: '/dashboard', pathMatch: 'full' }
]

@NgModule({
  imports: [ RouterModule.forRoot(routes,
      {
         onSameUrlNavigation: 'reload'
       }
   )],
  exports: [RouterModule]
})
export class AppRoutingModule {}
