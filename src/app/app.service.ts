import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http'
import { map, tap, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Item } from './components/item/item.model'

@Injectable({
  providedIn: 'root'
})

export class AppService {
  items: Item[]
  url2 = 'http://localhost:3000/api/'
  url = 'https://herokuprojectalan.herokuapp.com/api/'
  options = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  };
  
  constructor(private http: HttpClient) {
    
  } 

  
  public getCategories() {
    return this.http.get(this.url + 'categories');
  }

  public getCategoryItems(categoryId) {
    return this.http.get(this.url + 'category/' + categoryId);
  }

  public getSpecificCategory(categoryId) {
    console.log(this.url + 'categoryinfo/' + categoryId)
    return this.http.get(this.url + 'categoryinfo/' + categoryId);
  }

  public updateSpecificCategory(categoryID, categoryprops) {
    console.log(this.url + 'category/' + categoryID, categoryprops)
    return this.http.put(this.url + 'category/' + categoryID, categoryprops.toString(), this.options).pipe(
      tap(
        data => console.log(data)
        // ,error => console.error(error)
      )
    )}

  public createCategory(categoryprops) {
    console.log(categoryprops)
    return this.http.post(this.url + 'category', categoryprops.toString(), this.options).pipe(
      tap(
        data => console.log(data)
        // ,error => console.error(error)
      )
    )}

  public deleteCategory(itemID) {
    return this.http.delete(this.url + 'category/' + itemID);
  }

  public getSpecificItem(itemID) {
    console.log(this.url + 'item/' + itemID)
    return this.http.get(this.url + 'item/' + itemID );
  }

  public updateSpecificItem(itemID, itemProps) {
    console.log(this.url + 'item/' + itemID, itemProps)
    return this.http.put(this.url + 'item/' + itemID, itemProps.toString(), this.options).pipe(
      tap(
        data => console.log(data)
        // ,error => console.error(error)
      )
    )
}
  public createItem(itemProps) {
    console.log(itemProps)
    return this.http.post(this.url + 'item', itemProps.toString(), this.options).pipe(
      tap(
        data => console.log(data)
        // ,error => console.error(error)
      )
    )
  }

  public deleteItem(itemID) {
    return this.http.delete(this.url + 'item/' + itemID);
  }

  public getAllItem() {
    return this.http.get<ApiResponse>(this.url + 'items').pipe(
      catchError(this.handleError)
    );
  }

  public getBrands() {
    return this.http.get(this.url + 'brands');
  }

/* Gear API */

  public getGearID(gearID) {
    return this.http.get(this.url + 'gear/' + gearID);
  }
  
  public getGear(id) {
    return this.http.get(this.url + 'gear/?user=' + id );
  }


  public updateGear(gearID, gearProps) {
    console.log(this.url + 'gear/' + gearID, gearProps)
    return this.http.put(this.url + 'gear/' + gearID, gearProps.toString(), this.options).pipe(
      tap(
        data => console.log(data)
      )
      )
    }
  public createGear(gearProps) {
    console.log(gearProps)
    return this.http.post(this.url + 'gear', gearProps.toString(), this.options).pipe(
      tap(
        data => console.log(data)
      )
    )
  }

  public deleteGear(gearID) {
    return this.http.delete(this.url + 'gear/' + gearID);
  }

  public createGearCategory(gearID, gearProps) {
    console.log(gearProps)
    console.log(this.url + 'gear/category/' + gearID)
    return this.http.post(this.url + 'gear/category/' + gearID, gearProps.toString(), this.options).pipe(
      tap(
        data => console.log(data + 'ew')
      )
    )
  }

  public createGearItem(gearID, gearProps) {
    console.log(gearProps)
    return this.http.post(this.url + 'gear/item/' + gearID, gearProps.toString(), this.options).pipe(
      tap(
        data => console.log(data)
      )
    )
  }

  public deleteGearCategory(gearID, gearProps) {
    return this.http.put(this.url + 'gear/category/' + gearID, gearProps.toString(), this.options).pipe(
      tap(
        data => console.log(data)
      )
    )
  }

  public deleteGearItem(gearID, gearProps) {
    console.log('hee')
    // console.log(this.url + 'gear/item/' + gearID)
    // return this.http.put(this.url + 'gear/item/' + gearID, gearProps.toString(), this.options)
    return this.http.put(this.url + 'gear/item/' + gearID, gearProps.toString(), this.options).pipe(
      tap(
        data => console.log(data)
      )
    )
  }


  private handleError(error: HttpErrorResponse) {
    console.log('handleError')
    return throwError(
      error.message || error.error.message
    )
  }
}

export interface ApiResponse {
  results: any[]
}

