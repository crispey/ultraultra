import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core'
import { MaterialModule } from './material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { HeaderComponent } from './components/header/header.component'
import { UsersModule } from './components/users/users.module'
import { LoginComponent } from './components/auth/login/login.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AlertModule } from './modules/alert/alert.module'
import { AlertService } from './modules/alert/alert.service'
import { RegisterComponent } from './components/auth/register/register.component'
import { HttpClientModule } from '@angular/common/http';
import { GearComponent } from './components/gear/gear.component';
import { GearListComponent } from './components/gear/gear-list/gear-list.component';
import { GearEditComponent } from './components/gear/gear-edit/gear-edit.component';
import { ItemDetailsComponent } from './components/item/item-details/item-details.component';
import { ItemComponent } from './components/item/item.component';
import { ItemListComponent } from './components/item/item-list/item-list.component';
import { ItemEditComponent } from './components/item/item-edit/item-edit.component';
import { CategoryEditComponent } from './components/item/category-edit/category-edit.component';
import { APP_BASE_HREF } from '@angular/common';
import { UsecaseComponent } from './components/usecase/usecase.component';

@NgModule({
  declarations: [AppComponent, DashboardComponent, HeaderComponent, LoginComponent, RegisterComponent, GearComponent, GearListComponent, GearEditComponent, ItemDetailsComponent, ItemComponent, ItemListComponent, ItemEditComponent, CategoryEditComponent, UsecaseComponent],
  imports: [
    BrowserAnimationsModule,
    MaterialModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AlertModule,
    // UsersModule must be before AppRoutingModule,
    // otherwise userroutes are overwritten by '**'.
    UsersModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'},
    
    // LoggedInAuthGuard,
    // AdminRoleAuthGuard,
    AlertService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
