import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { AppService } from '../../../app.service'
import { Location } from '@angular/common'

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit {

  itemId
  items 

  constructor(private appService: AppService, private route: ActivatedRoute, private router: Router, private location: Location) {
    this.itemId = this.route.snapshot.paramMap.get('id');
  }
  ngOnInit() {
    this.appService.getSpecificItem(this.itemId).subscribe((data) => {
      this.items = data['item']
      console.log(this.items)
    })
  }
  onGoBack() {
    this.location.back();
  }
}
