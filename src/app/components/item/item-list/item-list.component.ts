import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Item } from '../item.model'
import { AppService } from '../../../app.service'
import { Location } from '@angular/common';


export interface Section {
  name: string;
  updated: Date;
}

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

  items
  categoryID
  categories
  isLoading = true

  constructor(
    private appService: AppService, 
    private route: ActivatedRoute, 
    private router: Router,
    private location: Location) {

  }



  ngOnInit() {
    this.categoryID = this.route.snapshot.paramMap.get('id');

    this.appService.getCategoryItems( this.categoryID).subscribe((data) => {
      this.items = data['items']
      this.isLoading = false;
    })
  }

  onImgError(event) { 
    event.target.src = 'https://www.metrorollerdoors.com.au/wp-content/uploads/2018/02/unavailable-image.jpg';
}

  onClickCard(itemID) {
    console.log(itemID)
    this.router.navigate(['item/', itemID])
    
  } 

  
  onClickAdd() {
    console.log("Add Item")
    this.router.navigate(['item/'+ 'new' +'/edit'])
  }

  onClickEdit(categoryID) {
    console.log(categoryID)
    this.router.navigate(['item/'+ categoryID +'/edit'])
  }

  
  onClickDelete(categoryID){
    this.appService.deleteItem(categoryID)
    .subscribe(data => {
      console.log(data)
    })
    this.router.navigate(['items']).then(a => {
      this.router.navigate(['items/' + this.categoryID])
    })
  }

  onGoBack() {
    this.location.back();
  }
}
