export class Item {
    _id: string
    name: string
    weight_gram: number
    cost: number
    description: string
    brand: {
        name?: string
    }
    category: {
        name?: string
    }
}
