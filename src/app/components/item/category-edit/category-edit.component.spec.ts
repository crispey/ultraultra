import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModule } from '../../../modules/shared.module';
import { CategoryEditComponent } from './category-edit.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const thiscategory = 'name'
const thisimage = 'image'

describe('CategoryEditComponent', () => {
  let component: CategoryEditComponent;
  let fixture: ComponentFixture<CategoryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryEditComponent ],
      imports: [ReactiveFormsModule, FormsModule, HttpClientTestingModule, SharedModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.form.valid).toBeFalsy();
  });

  it('form category filled', () => {
    const name = component.form.controls[thiscategory];
    name.setValue('anything')
    expect(name.valid).toBeTruthy();
  });
  
  it('form not fully filled fails', () => {
    const name = component.form.controls[thiscategory];
    name.setValue('anything')
    expect(component.form.valid).toBeFalsy();
  });

  it('form fully filled', () => {
    const name = component.form.controls[thiscategory];
    const image = component.form.controls[thisimage];
    name.setValue('anything')
    image.setValue('anything')
    expect(component.form.valid).toBeTruthy();
  });
});
