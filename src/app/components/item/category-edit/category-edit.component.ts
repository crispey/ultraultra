import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Category } from '../category.model'
import { AppService } from '../../../app.service'
import { NgForm, FormBuilder, FormGroup } from '@angular/forms'
import { Location } from '@angular/common'

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.scss']
})
export class CategoryEditComponent implements OnInit {

  title: string
  editMode: boolean
  id: number
  category
  categoryId

  form: FormGroup;

  formName: string;
  formImage: string;

  constructor(private appService: AppService, private route: ActivatedRoute, private router: Router, private location: Location, private formBuilder: FormBuilder) {
    this.categoryId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {


    if(this.categoryId !== undefined && this.categoryId !== null && this.categoryId !== 'new') {


      this.appService.getSpecificCategory(this.categoryId).subscribe((data) => {
        this.category = data['categories']

      })
      this.editMode = true; 
    } else {
      this.editMode = false; 
    }

    this.form = this.formBuilder.group({
      name: '',
      image: ''  
    })

    if(this.editMode){
      console.log('editmode')
      this.getData()
    }else{
      console.log('addmode')
    }
  }

  getData() {
    this.appService.getSpecificCategory(this.categoryId).subscribe((data) => {
      this.category = data['categories']
      this.form.patchValue({
        name: this.category.name,
        image: this.category.image
      })
    })
  }

  onSaveCategory(){

    let body = new URLSearchParams();
    body.set('name', this.form.controls['name'].value);
    body.set('image', this.form.controls['image'].value);

    if(!this.editMode) {
      
      console.log('createCategory')
      // Save category
      this.appService.createCategory(body)
      .subscribe(data => {
        this.router.navigate(['category'])
      })
    } else {
      console.log('updateCategory')
      this.appService.updateSpecificCategory(this.categoryId, body)
      .subscribe(data => {
        console.log(data)
        this.router.navigate(['category'])
      })
    }

  }

  onDeleteCategory(){
    this.appService.deleteCategory(this.categoryId)
    .subscribe(data => {
      console.log(data)
      this.router.navigate(['category'])
    })
  }


  onGoBack() {
    this.location.back();
  }
}
