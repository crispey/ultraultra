import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Item } from './item.model'
import { AppService } from '../../app.service'
import { Location } from '@angular/common';
import { debug } from 'util';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})


export class ItemComponent implements OnInit {

categories

  constructor(
    private appService: AppService, 
    private route: ActivatedRoute, 
    private router: Router,
    private location: Location) {

  }
  onImgError(event) { 
    event.target.src = 'https://www.metrorollerdoors.com.au/wp-content/uploads/2018/02/unavailable-image.jpg';
}

  ngOnInit() {
    
    this.appService.getCategories().subscribe((data) => {
      this.categories = data['categories']
    })
  }

  onClickAdd() {
    console.log("Add Category")
    this.router.navigate(['category/'+ 'new' +'/edit'])
  }

  onClickEdit(categoryID) {
    console.log(categoryID)
    this.router.navigate(['category/'+ categoryID +'/edit'])
  }

  onClickCard(categoryID) {
    console.log(categoryID)
    this.router.navigate(['items/', categoryID])
  }
  
  onDeleteCategory(categoryID){
    this.appService.deleteCategory(categoryID)
    .subscribe(data => {
      console.log(data)
      this.router.navigate(['category'])
    })
  }

  onGoBack() {
    this.location.back();
  }
}
