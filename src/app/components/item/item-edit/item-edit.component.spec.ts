import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemEditComponent } from './item-edit.component';
import { SharedModule } from '../../../modules/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const thiscategory = 'category'
const thisname = 'name'
const thisshort_description = 'short_description'

describe('ItemEditComponent', () => {
  let component: ItemEditComponent;
  let fixture: ComponentFixture<ItemEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemEditComponent ],
      imports: [ReactiveFormsModule, FormsModule, HttpClientTestingModule, SharedModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('form invalid when empty', () => {
    expect(component.form.valid).toBeFalsy();
  });

  it('form category filled', () => {
    const category = component.form.controls[thiscategory];
    category.setValue('anything')
    expect(category.valid).toBeTruthy();
  });
  
  it('form not fully filled fails', () => {
    const category = component.form.controls[thiscategory];
    category.setValue('anything')
    expect(component.form.valid).toBeFalsy();
  });

  it('form short description too long ', () => {
    const category = component.form.controls[thiscategory];
    const name = component.form.controls[thisname];
    const short_description = component.form.controls[thisshort_description];
    short_description.setValue('ThisLineIsLongerThanTwentyCharacters')
    category.setValue('anything')
    name.setValue('anything')
    expect(component.form.valid).toBeFalsy();
  });

  it('form fully filled', () => {
    const category = component.form.controls[thiscategory];
    const name = component.form.controls[thisname];
    const short_description = component.form.controls[thisshort_description];
    short_description.setValue('anything')
    category.setValue('anything')
    name.setValue('anything')
    expect(component.form.valid).toBeTruthy();
  });
});
