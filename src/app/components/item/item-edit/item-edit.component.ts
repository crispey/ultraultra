import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Category } from '../category.model'
import { Item } from '../item.model'
import { AppService } from '../../../app.service'
import { NgForm, FormBuilder, FormGroup } from '@angular/forms'
import { Location } from '@angular/common'

@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.scss']
})
export class ItemEditComponent implements OnInit 
{

  title: string
  editMode: boolean
  id: number
  categories
  item
  brands
  itemId

  form: FormGroup;

  constructor(private appService: AppService, private route: ActivatedRoute, private router: Router, private location: Location, private formBuilder: FormBuilder) {
    this.itemId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {

    this.appService.getCategories().subscribe((data) => {
      this.categories = data['categories']
    })

    this.appService.getBrands().subscribe((data) => {
      this.brands = data['brands']
    })


    if(this.itemId !== undefined && this.itemId !== null && this.itemId !== 'new') {

      this.appService.getSpecificItem(this.itemId).subscribe((data) => {
        this.item = data['item']
      })

      this.editMode = true; 
    } else {
      this.editMode = false; 

      
    }

    this.form = this.formBuilder.group({
      name: '',
      weight_gram: 0,
      description: '',
      cost: 0,
      short_description: '',
      category: '',
      brand: '',
      image: ''
    })

    if(this.editMode){
      console.log('editmode')
      this.getData()
    }else{
      console.log('addmode')
    }
  }

  getData() {
    this.appService.getSpecificItem(this.itemId).subscribe((data) => {
      this.item = data['item'] 
      console.log(this.item)
      this.form.patchValue({
        name: this.item[0].name,
        weight_gram: this.item[0].weight_gram,
        description: this.item[0].description,
        cost: this.item[0].cost,
        short_description: this.item[0].short_description,
        category: this.item[0].category,
        brand: this.item[0].brand,
        image: this.item[0].image
      })

    })
  }

  onSaveItem(){

    let body = new URLSearchParams();
    if(this.form.controls['name'].value !== undefined){
      body.set('name', this.form.controls['name'].value);
    }
    if(this.form.controls['weight_gram'].value !== undefined){
      body.set('weight_gram', this.form.controls['weight_gram'].value);
    }
    if(this.form.controls['cost'].value !== undefined ){
      body.set('cost', this.form.controls['cost'].value);
    }
    if(this.form.controls['description'].value !== undefined){
      body.set('description', this.form.controls['description'].value);
    }
    if(this.form.controls['short_description'].value !== undefined){
      body.set('short_description', this.form.controls['short_description'].value);
    }
    if(this.form.controls['category'].value !== undefined){
      body.set('category', this.form.controls['category'].value);
    }
    if(this.form.controls['brand'].value !== undefined && this.form.controls['brand'].value != ''){
      body.set('brand', this.form.controls['brand'].value);
    }
    if(this.form.controls['image'].value !== undefined){
      body.set('image', this.form.controls['image'].value);
    }


    if(!this.editMode) {
      
      console.log('createItem')
      // Save category
      this.appService.createItem(body)
      .subscribe(data => {
        this.location.back();
      })
    } else {
      console.log('updateItem')
      this.appService.updateSpecificItem(this.itemId, body)
      .subscribe(data => {
        console.log(data)
        this.location.back();
      })
    }

  }

  onGoBack() {
    this.location.back();
  }
}