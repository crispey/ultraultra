import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { AppService } from '../../../app.service'
import { Location } from '@angular/common';
import { Observable, Subscription } from 'rxjs'
import { AuthService } from '../../auth/auth.service'

@Component({
  selector: 'app-gear-list',
  templateUrl: './gear-list.component.html',
  styleUrls: ['./gear-list.component.scss']
})
export class GearListComponent implements OnInit {

  gearID
  gear
  userID
  fullName
  email
  emailSubscription: Subscription

  constructor(
    private appService: AppService, 
    private authService: AuthService,
    private route: ActivatedRoute, 
    private router: Router,
    private location: Location) {

    this.email = authService.userEmail['_value']
    console.log(this.email)
    if(this.email != null){
      this.appService.getGear(this.email).subscribe((data) => {
        this.gear = data['gear']
        console.log(this.gear)
      })
    }
    this.emailSubscription = this.authService.userEmailSub.subscribe(name => (this.fullName = name))
  }
  ngOnInit() {

  }

  checkSame(item2){
    this.gearID = this.route.snapshot.paramMap.get('id');
    if(this.gearID == item2) { return true }
  }

  addGear(){
    this.router.navigate([`gear/${this.gearID}/edit`])
  }


  onGoBack() {
    this.location.back();
  }
}
