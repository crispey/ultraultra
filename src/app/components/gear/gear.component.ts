import { Component, OnInit } from '@angular/core'
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized, ActivatedRoute } from '@angular/router';
import { AppService } from '../../app.service'
import { Location } from '@angular/common';
import { Observable, Subscription } from 'rxjs'
import { AuthService } from '../auth/auth.service'
import { KeyValue } from '@angular/common';
import { NgForm, FormBuilder, FormGroup } from '@angular/forms'
import { state } from '@angular/animations';



@Component({
  selector: 'app-gear',
  templateUrl: './gear.component.html',
  styleUrls: ['./gear.component.scss'],
})

export class GearComponent implements OnInit {

  gearID
  itemID
  gear
  gearcheck
  allitems
  total

  form: FormGroup;
  form2: FormGroup;

 constructor(
    private appService: AppService, 
    private authService: AuthService,
    private route: ActivatedRoute, 
    private router: Router,
    private formBuilder: FormBuilder,
    private location: Location) {
      router.events.forEach((event) => {
        if(event instanceof NavigationEnd) {

          this.gearID = this.route.snapshot.paramMap.get('id')
          console.log('now' + this.gearID)
          if(this.gearID != null){
          this.appService.getGearID(this.gearID).subscribe((data) => {
            this.gear = data['gear']
            console.log(this.gear)    
          })
          this.appService.getAllItem().subscribe((data) => {
            this.allitems = data['items']
            console.log(this.allitems)
          })
        }
      }
    });
  }



  ngOnInit() {
    this.form = this.formBuilder.group({
      item: ''
    })
    this.form2 = this.formBuilder.group({
      categorytitle: ''
    })
    if(this.gearID != null){
    this.appService.getGearID(this.gearID).subscribe((data) => {
      this.gearcheck = data['gear'][0]['categories']
      

      console.log(this.gearcheck)    
    })
  }
  }

  onSelectItem(itemID) {
    console.log(itemID)
    this.router.navigate(['item/', itemID])
  }

  removeItem(category, item) {
    let body = new URLSearchParams();
    body.set('categoryid', category);
    body.set('itemid', item);
    
    this.appService.deleteGearItem(this.gearID, body).subscribe(data => {
      console.log(data)
    })
    this.router.navigate(['gear']).then(a => {
      this.router.navigate([`gear/${this.gearID}`])
    })
  }

  deleteGear() {
    console.log('deleteGear')
    this.appService.deleteGear(this.gearID)
    .subscribe(data => {
      console.log(data)
    })
    this.router.navigate(['gear']).then(a => {
      this.router.navigate([`gear/`])
    })
  }


  deleteCategory(categoryid) {
    let body = new URLSearchParams();
    body.set('categoryid', categoryid);
    this.appService.deleteGearCategory(this.gearID, body).subscribe(data => {
      console.log(data)
    })
    this.router.navigate(['gear']).then(a => {
      this.router.navigate([`gear/${this.gearID}`])
    })
  }

  addItem(categoryid) {
    var itemid = (this.form.controls['item'].value)
    let body = new URLSearchParams();
    body.set('categoryid', categoryid);
    body.set('itemid', itemid);
    this.appService.createGearItem(this.gearID, body).subscribe(data => {
      console.log(data)

    })
    this.router.navigate(['gear']).then(a => {
      this.router.navigate([`gear/${this.gearID}`])
    })
    

  }

  addCategory() {
    var categoryname = (this.form2.controls['categorytitle'].value)
    let body = new URLSearchParams();
    body.set('categoryname', categoryname );
    this.appService.createGearCategory(this.gearID, body).subscribe(data => {
      console.log(data)
    })
    this.router.navigate(['gear']).then(a => {
      this.router.navigate([`gear/${this.gearID}`])
    })
  }

  onPage(){
    if(this.gearID != '' && this.gearID != null  && this.gearID != undefined )
    return true
  }

  onGoBack() {
    this.location.back();
  }
}
