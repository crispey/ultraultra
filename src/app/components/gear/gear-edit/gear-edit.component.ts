import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { AppService } from '../../../app.service'
import { NgForm, FormBuilder, FormGroup } from '@angular/forms'
import { Location } from '@angular/common'
import { AuthService } from '../../auth/auth.service'

@Component({
  selector: 'app-gear-edit',
  templateUrl: './gear-edit.component.html',
  styleUrls: ['./gear-edit.component.scss']
})
export class GearEditComponent implements OnInit {


  userId
  editMode
  gear
  gearId
  form: FormGroup;

  constructor(private appService: AppService, private route: ActivatedRoute, private router: Router, private location: Location, private formBuilder: FormBuilder, private authService: AuthService) {
    this.gearId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.userId = this.authService.userEmail['_value']
    if(this.gearId !== undefined && this.gearId !== null && this.gearId !== 'new') {
      this.appService.getGearID(this.gearId).subscribe((data) => {
        this.gear = data['item']
      })

      this.editMode = false; 
    } else {
      this.editMode = false; 

      
    }

    this.form = this.formBuilder.group({
      title: '',
      user: ''
    })

    if(this.editMode){
      console.log('editmode')
      this.getData()
    }else{
      console.log('addmode')
    }
  }

  getData() {
  
  }

  onSaveGear(){

    let body = new URLSearchParams();
    if(this.form.controls['title'].value !== undefined){
      body.set('title', this.form.controls['title'].value);
    }
    
      body.set('user', this.userId);
    
    

    if(!this.editMode) {
      
      console.log('createGear')
      // Save category
      this.appService.createGear(body)
      .subscribe(data => {
        this.location.back();
      })
    } else {
      console.log('updateGear')
     
    }

  }

  onGoBack() {
    this.location.back();
  }

}
