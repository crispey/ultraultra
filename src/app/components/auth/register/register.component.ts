import { Component, OnInit, OnDestroy } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { AuthService } from '../../auth/auth.service'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup
  subs: Subscription

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.registerForm = new FormGroup({
    email: new FormControl(null, [Validators.required, this.validEmail.bind(this)]),
    password: new FormControl(null, [Validators.required, this.validPassword.bind(this)]),
    firstname: new FormControl(null, [Validators.required]),
    lastname: new FormControl(null, [Validators.required])
  })
  this.subs = this.authService.userIsLoggedIn.subscribe(alreadyLoggedIn => {
    if (alreadyLoggedIn) {
      console.log('User already logged in > to dashboard')
      this.router.navigate(['/dashboard'])
    }
  })
}
  ngOnDestroy() {
    if (this.subs) {
      this.subs.unsubscribe()
    }
  }

  onSubmit() {
    if (this.registerForm.valid) {
      let object = 
        {name: 
          {
          'firstname': this.registerForm.value.firstname,
          'lastname': this.registerForm.value.lastname
        },
        'password': this.registerForm.value.password,
        'email': this.registerForm.value.email
      }

      this.authService.registerUser(object).subscribe((data) => {
        console.log(data)
      })
    } else {
      console.error('loginForm invalid')
    }
  }

  validEmail(control: FormControl): { [s: string]: boolean } {
    const email = control.value
    const regexp = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    if (regexp.test(email) !== true) {
      return { email: false }
    } else {
      return null
    }
  }

  validPassword(control: FormControl): { [s: string]: boolean } {
    const password = control.value
    const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}')
    const test = regexp.test(password)
    if (regexp.test(password) !== true) {
      return { password: false }
    } else {
      return null
    }
  }
}