import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router'
import { Observable } from 'rxjs'
import { AuthService } from '../auth/auth.service'
import { Router } from '@angular/router'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(private alertService: AlertService, private authService: AuthService, private router: Router) {}
  isLoggedIn$: boolean

  canActivate( 
       route: ActivatedRouteSnapshot,
       router: RouterStateSnapshot
       ): boolean | UrlTree | Promise<boolean | UrlTree> | Observable<boolean | UrlTree> {
    this.authService.userIsLoggedIn.subscribe(response => this.isLoggedIn$ = response)

    if(!this.isLoggedIn$){
      this.alertService.error('No access!')
      return this.router.createUrlTree(['/login'])
    }
    return this.authService.userIsLoggedIn
  }
}
